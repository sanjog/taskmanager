(function () {
	$.post("list.php","",function(response){
		$("#file-list").html(response)
	});
	var input = document.getElementById("files"), 
		formdata = false;

	function showUploadedItem () {
  		$.post("list.php","",function(response){
		$("#file-list").html(response)
	});
	}   

	if (window.FormData) {
  		formdata = new FormData();
  		document.getElementById("btn").style.display = "none";
	}
	
 	input.addEventListener("change", function (evt) {
 		document.getElementById("response").innerHTML = "Uploading . . ."
 		var i = 0, len = this.files.length, img, reader, file;
	
		for ( ; i < len; i++ ) {
			file = this.files[i];
				var ext=file.name.split('.').pop();
			if ($.inArray(ext, ['gif','png','jpg','jpeg','pdf','doc','docx']) != -1) {
				if (formdata) {
					formdata.append("images[]", file);
					$.ajax({
				url: "upload.php",
				type: "POST",
				data: formdata,
				processData: false,
				contentType: false,
				success: function (res) {
					$("#response").html(res); 
					 showUploadedItem();
				}
			});
				}
			}else{
				document.getElementById("response").innerHTML = ""
				alert("invalid file type");
			}	
		}
	}, false);
}());
