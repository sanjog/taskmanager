<?php
 function site_url($site){
   $sitename= explode("/",$site);
   if(sizeof($sitename) !=2){
       die("not a valid argument ".$site);
   }else{
       return BASE_URL."index.php?m=".$sitename[0]."&a=".$sitename[1];
   }
}
function base_url(){
    return BASE_URL;
}
function assets_url(){
    return BASE_URL."assets/";
}
function date_readable($datestring){
    return date('d/m/Y', strtotime($datestring));
}