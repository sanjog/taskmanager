<?php
require_once 'vendor/class.phpmailer.php';
class Controller {

    public $db;

    function __construct() {
	if(ALLOWED_IP==" "){
            
        }else{
            $status=0;
            $ip_list=explode(",",ALLOWED_IP);
            foreach($ip_list as $key=>$ip){
                if($_SERVER["REMOTE_ADDR"]==$ip){
                    $status=1;break;
                }else{
                    $status=0;
                }
            }
            if($status ==0){
                $url=BASE_URL."denied.php";
                header("location: $url");
            }else{
                
            }
            
            
        }
        $this->db = new Database();
    }

    function load_model($model_name) {
        $model_name = strtolower($model_name);
        if (file_exists(APPPATH . 'models/' . $model_name . ".php")) {
            require_once APPPATH . 'models/' . $model_name . ".php";

            $classname = ucfirst($model_name) . "_model";
            if (class_exists($classname)) {
                $this->$model_name = new $classname();
            } else {
                die("Cant find Class '" . $classname . "' in " . APPPATH . 'models/' . $model_name . ".php");
            }
        } else {
            die("cant find model file " . APPPATH . 'models/' . $model_name . ".php");
        }
    }

    function load_view($view_name, $data = array()) {
        foreach ($data as $key => $value) {
            $$key = $value;  
        }
        $global=$data;
        include_once APPPATH . 'views/' . $view_name . ".php";
    }

    function site_url($site) {
        $sitename = explode("/", $site);
        if (sizeof($sitename) != 2) {
            die("not a valid argument " . $site);
        } else {
            return BASE_URL . "index.php?m=" . $sitename[0] . "&a=" . $sitename[1];
        }
    }

    function base_url() {
        return BASE_URL;
    }

    function assets_url() {
        return BASE_URL . "assets/";
    }
function redirect($site) {
     $sitename = explode("/", $site);
        if (sizeof($sitename) != 2) {
            die("not a valid argument " . $site);
        } else {
            $url= BASE_URL . "index.php?m=" . $sitename[0] . "&a=" . $sitename[1];
             header("location:$url");
        }
       
        exit;
    }

    function format_date($date) {
        list($d, $m, $y) = preg_split('/[\/.-]/', $date);
        $new_date = "$y-$m-$d";
        return $new_date;
    }

    function date_format($date) {
        list($y, $m, $d) = preg_split('/[\/.-]/', $date);
        $new_date = "$d/$m/$y";
        return $new_date;
    }

    function pr($data) {
        print "<pre>";
        print_r($data);
        print "</pre>";
    }
    
    function sendMail($heading,$body,$emails) {
      $message=  "<!DOCTYPE html>
<html lang=\"en\">

<head>

<style type=\"text/css\">
html{
	margin:0;
	padding:0;
	height: auto;
	width: auto;
}

body {
	padding: 0;
	margin:auto;
	min-height: 800px;
	height: auto;
	width:800px;
}

/********Hero Units*******/
#hero-unit {
	padding-top:25px;
	position: relative;
	height:70px;
	margin:auto;
}

#hero-unit .date {
	position: absolute;
	bottom: 8px;
	right:0px;
	font-family: 'Helvetica Neue';
	font-size: 17px;
	color:black;
	font-weight: lighter;
}

#hero-unit h1 {
	font-family: 'Arial';
	font-size:50px;
	color: #8f8f8f;
	font-weight: lighter;
	padding-left: 70px;
	margin:0;
	letter-spacing: 2px;
}

#hero-unit .boh {
	font-size: 48px;
	color:black;
	font-family: 'Helvetica Neue';
}

#hero-unit .storenumber {
	font-size: 38px;
	color:black;
	font-family: 'Helvetica Neue';
}

/*****Group stuff*****/
#group {
	width:100%;
	height: auto;
}

.line {
	position:relative;
	width:100%;
	height:8px;
	border-top:1px solid black;
	background-color: #2c2c2c;	
}

.row2 {
	position:relative;
	width:100%;
	float:right;
	margin:auto;	
	padding-bottom: 5px;
}

.header {
	position:absolute;
	top:0px;
	left:90px;
	font-family: 'Helvetica Neue';
	font-weight: lighter;
	font-color:#726c6f;
	font-size: 25px;
	letter-spacing:3px;
	color: #727272;
	line-height: 30px;
}

#square {
	width: 45px;
	height: 35px;
	background: #2c2c2c;
	position:absolute;
	top:0;
	text-align: right;
	font-size: 32px;
	line-height: 34px;
	font-family: 'Helvetica Neue';
	font-weight: bold;
	color:white;
	padding:0;
	margin:0;
}


#triangle-topleft {
	width: 0px;
	height: 0;
	border-top: 35px solid #2c2c2c;
	border-right:35px solid transparent;
	position: absolute;
	top:0;
	left:45px;
}


.row3 {
	position: relative;
	height:auto;
	padding: 12px 0 12px 0;
	top:8px;
	min-height:290px;
	height: auto;
}

#triangle-topleft2 {
	position: absolute;
	top:0;
	left:0;
	width: 0;
	height: 0;
	border-top: 45px solid white;
	border-right: 45px solid transparent;
	
}

.notes {
	position:relative;
	top:0px;
	left:25px;
	background-color: #e5e5e5;
	width:700px;
	min-height: 220px;
	height: auto;
	margin:auto;
	padding:20px 14px 14px 40px;
	font-family: 'Helvetica Neue';
	font-size: 18px;
	line-height: 24px;
}
</style>

</head>

<body>

		<div id=\"hero-unit\">
			 <h1><span class=\"boh\">Task Manager</span></h1> 
			<div class=\"date\"><span contenteditable=\"true\">Date: ".date("d-m-Y")." </span></div>
		</div>
		
		<div id=\"group\">
			<div class=\"line\"></div>
			<div class=\"row2\">
				<div id=\"square\"><b>:)</b></div><div id=\"triangle-topleft\"></div> <span class=\"header\">".$heading."</span>
			</div>
			
			<br />
			
			<div class=\"row3\">
				<div class=\"notes\">
					<div id=\"triangle-topleft2\"></div>
					<span contenteditable=\"true\">
					".$body."
					</span>
                                        <br>
                                        <br>
                                        <br>
                                        <div>
                                        Team,<br>
                                        <b>Task Manager</b>
                                        </div>
					</div>
			</div>
		</div>
		<!--End of Group-->
		
		<br />
		
												
</body>
</html>";
        $mail = new PHPMailer();
//Tell PHPMailer to use SMTP
        $mail->IsSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
        $mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
//Set the hostname of the mail server
        $mail->Host = 'smtp.auroin.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        //$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
     //   $mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
        $mail->SMTPAuth = false;
//Set who the message is to be sent from
        $mail->SetFrom('arcweb.task.manager@gmail.com', 'Support@TaskManager');
//Set who the message is to be sent to
        foreach($emails as $email){
            $mail->AddAddress($email['email'], $email['first_name']. " ".$email['last_name']);
        }
//Set the subject line
        $mail->Subject = 'Task Manager Notification';
//Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
        $mail->MsgHTML($message);


//Send the message, check for errors
        if (!$mail->Send()) {
           // echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
           
        }
    }
    

}
