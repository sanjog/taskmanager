<?php

class Model{
    public $db;
    function __construct(){
         $this->db = new Database();
    }
      function format_date($date) {
        list($d, $m, $y) = preg_split('/[\/.-]/', $date);
        $new_date = "$y-$m-$d";
        return $new_date;
    }

    function date_format($date) {
        list($y, $m, $d) = preg_split('/[\/.-]/', $date);
        $new_date = "$d/$m/$y";
        return $new_date;
    }

    function pr($data) {
        print "<pre>";
        print_r($data);
        print "</pre>";
    }
}