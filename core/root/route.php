<?php
require_once APPPATH.'config/config.php';
require BASEPATH.'root/controller.php';
require BASEPATH.'root/model.php';
require BASEPATH.'db/database.php';
require BASEPATH.'helper/helper.php';
foreach($config as $key => $value){
    define(strtoupper($key),$value);
}
if (isset($_GET['m'])) {
    $filename = APPPATH . 'controllers/' . $_GET['m'] . '.php';
    route($filename,$_GET['m']);
    
}else{
     $filename = APPPATH . 'controllers/' . $config['default_controller'] . '.php';
     route($filename,$config['default_controller']);
}
function route($filename,$request_class){
    if (file_exists($filename)) {
        require_once $filename;
        $classname = ucfirst($request_class);
        if (class_exists($classname)) {
            $obj = new $classname();
            if (isset($_GET['a'])) {
                $func = $_GET['a'];
                if (method_exists($obj, $func)) {
                    $obj->$func();
                } else {
                    die("Could not find function '" . $func . "' in the class '" . $classname . " ' ");
                }
            } else {
                if(method_exists($obj, 'index')){
                  $obj->index();
                }else{
                    die("Could not find function 'index' in the class '" . $classname . " ' ");
                }
            }
        }else{
            die("class does not found '".$classname."'");
        }
    } else {
        die("file not exist '" . $filename."'");
    }
}