<?php
class Project_plan_model extends Model{
    function __construct() {
        parent::__construct();
    }
    function insertdata($data){
       return $this->db->insert("project_plan",$data);
    }
    function retrivedata(){
        return $this->db->select("project_plan");
    }
    function nameById($plan_id){
        $cond="id=$plan_id";
        $data=$this->db->select("project_plan",$cond);
        
    return $data[0]['project_plan'];
        
    }
}