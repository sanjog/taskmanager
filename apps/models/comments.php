<?php
class Comments_model extends Model{
    function __construct() {
        parent::__construct();
    }
    function getComments($id){
        $sql = "SELECT a.id,a.user_id, a.comments, a.posted_time, b.project_name, c.first_name, c.last_name
FROM comments a, projects b, user c
WHERE a.project_id =$id
AND a.status =1
AND a.project_id = b.id
AND a.user_id = c.id
ORDER BY a.posted_time DESC ";
        return $this->db->customSelect($sql);
    }
    function saveComments($data){
        $this->db->insert("comments",$data);
    }
    function deleteComments($id){
        $cond="id=".$id;
        $data=$this->db->select("comments",$cond);
        $currenttime = time();
        $timestamp = strtotime($data[0]["posted_time"]);
        $timediff=$currenttime-$timestamp;
        if($timediff <= 300){
                    return $this->db->delete("comments",$cond);

        }else{
            return 0;
        }
    }
    
}