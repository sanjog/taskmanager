<?php

class Projects_model extends Model {

    function __construct() {
        parent::__construct();
    }

    function insertdata($data) {
        return $this->db->insert("projects", $data);
    }
    function updatedata($data,$id){
        $cond="id=$id";
        $this->db->update("projects",$data,$cond);
    }
    function retrivedata() {
        if($_SESSION["emptype"]==3){
        $sql = "select a.id,a.project_name,a.client_code,a.status,a.date_created,
             c.first_name,c.last_name
             FROM projects a,user c WHERE
             a.assigned_by=c.id AND a.delete_status=1 AND a.assigned_by=".$_SESSION["user_id"]." ORDER BY a.date_created DESC ";     
        }else{
             $sql = "select a.id,a.project_name,a.client_code,a.status,a.date_created,
             b.project_plan,c.first_name,c.last_name
             FROM projects a,project_plan b,user c WHERE
             a.project_plan=b.id AND a.assigned_by=c.id AND a.delete_status=1 ORDER BY a.date_created DESC ";
        }

        
        return $this->db->customSelect($sql);
    }

    function retriveProjectById($id) {
        $sql = "SELECT a.id, a.project_name, a.client_code, a.status, a.date_created,a.assigned_by, a.project_description, a.project_url,a.ftp_info, a.cms_info, a.controlpanel_info, a.other_info, b.project_plan, c.first_name, c.last_name
FROM projects a, project_plan b, user c
WHERE a.project_plan = b.id
AND a.assigned_by = c.id
AND a.id =$id";
        $data=$this->db->customSelect($sql);
        return $data[0];
    }
    function retriveProject($id){
        $cond="id=$id";
        $data=  $this->db->select("projects",$cond);
        return $data;
    }
    function trashProject($data,$id){
        $cond="id=$id";
        $this->db->update("projects",$data,$cond);
    }
    function getEmails($id){
        $sql="select email,first_name,last_name from user where emptype=1 OR emptype=2 OR id=$id";
	
        $data=  $this->db->customSelect($sql);
	return $data;
    }

}
