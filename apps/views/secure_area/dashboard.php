<div id="content" class="span10">
    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-book"></i> Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Project Name</th>
                            <th>Date Created</th>
                            <th>Assigned By</th>
                            <th>Client Code</th>
                            
                            <th>Project Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach($projects as $project) {?>
                        <tr>
                            <td><?php echo $project['id']; ?></td>
                            <td><?php echo $project['project_name']; ?></td>
                            <td class="center"><?php echo date_readable($project['date_created']); ?></td>
                            <td class="center"><?php echo $project['first_name']." ".$project['last_name']; ?></td>
                            <td class="center"><?php echo $project['client_code']; ?></td>
                        
                            <td>
                                <?php if($project['status']==1){ ?>
                                 <span class="label label-info">
                                    Not yet started
                                </span>
                                <?php } ?>
                                <?php if($project['status']==2){ ?>
                                 <span class="label label-important">
                                    Progress
                                </span>
                                <?php } ?>
                                <?php if($project['status']==3){ ?>
                                 <span class="label label-inverse">
                                    Need more info
                                </span>
                                <?php } ?>
                                <?php if($project['status']==4){ ?>
                                 <span class="label label-warning">
                                    Paused
                                </span>
                                <?php } ?>
                                <?php if($project['status']==5){ ?>
                                 <span class="label label-success">
                                    Done
                                </span>
                                <?php } ?>
                            </td>
                            <td class="center">
                                <a class="btn btn-small btn-success" href="<?php echo site_url('projects/viewProject')?>&pid=<?php echo $project['id'];?>">
                                    <i class="icon-zoom-in icon-white"></i>  
                                    View                                            
                                </a>
                                <a class="btn btn-small btn-info" href="<?php echo site_url('projects/editProject')?>&pid=<?php echo $project['id'];?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>
                                <a class="btn btn-small btn-danger" onclick="return confirm('Do you want to delete')"  href="<?php echo site_url('projects/trashProject')?>&pid=<?php echo $project['id'];?>">
                                    <i class="icon-trash icon-white"></i> 
                                    Trash
                                </a>
                            </td>
                        </tr>
                        <?php  } ?>

                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
</div><!--/#content.span10-->