<div id="content" class="span10">
    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-book"></i> Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Employee Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>User Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php foreach($users as $user) {?>
                        <tr>
                            <td><?php echo $user['empcode']; ?></td>
                            <td class="center"><?php echo $user['first_name']." ".$user['last_name']; ?></td>
                            <td class="center"><?php echo $user["email"] ?></td>
                            <td class="center"><?php if($user["emptype"] == 1){
                                echo "<span class='label label-info'>Admin</span>";
                            }
                            if($user["emptype"] == 2){
                                  echo "<span class='label label-important'>Developer</span>";
                            }
                            if($user["emptype"] == 3){
                               echo "<span class='label label-warning'>Project Manager</span>";
                            }
                                 ?></td>
                             <td class="center">
                                <a class="btn btn-small btn-success" href="<?php echo site_url('projects/viewProject')?>&pid=<?php echo $user['id'];?>">
                                    <i class="icon-zoom-in icon-white"></i>  
                                    View                                            
                                </a>
                                <a class="btn btn-small btn-info" href="<?php echo site_url('user/edit')?>&uid=<?php echo $user['id'];?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>
                                <a class="btn btn-small btn-danger" onclick="return confirm('Do you want to delete')" href="<?php echo site_url('user/trashProject')?>&uid=<?php echo $user['id'];?>">
                                    <i class="icon-trash icon-white"></i> 
                                    Trash
                                </a>
                            </td>
                        </tr>
                        <?php  } ?>

                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
</div><!--/#content.span10-->