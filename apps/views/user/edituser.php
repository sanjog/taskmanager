<div id="content" class="span10">
    <div class="row-fluid sortable">
       
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-book"></i> Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
              <div class="box-content">
              <form class="form-horizontal" method="post" action="<?php echo site_url('user/update'); ?>">
                    <fieldset>
                        <legend>Add New User</legend>
                        <?php if(isset($_SESSION['error_msg'])){?>
                         <div class="alert alert-info">
                        <?php
                        $msg=$_SESSION["error_msg"];
                        echo $msg;
                        unset($_SESSION["error_msg"]);
                        ?>
                         </div>
                        <?php } ?>
                        <?php if(isset($_SESSION["success_msg"])){?>
                         <div class="alert alert-success">
                        <?php 
                        $msg=$_SESSION["success_msg"];
                        echo $msg;
                        unset($_SESSION["success_msg"]);
                        ?>
                         </div>
                        <?php } ?>
                         <div class="control-group">
                            <label class="control-label" for="user[first_name]">First Name</label>
                            <div class="controls">
                                <input type="text" name="user[first_name]" value="<?php echo  $user["first_name"];?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="user[last_name]">Last Name</label>
                            <div class="controls">
                                <input type="text" name="user[last_name]" value="<?php echo  $user["last_name"];?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="user[empcode]">Employee Code</label>
                            <div class="controls">
                                <input type="text"  name="user[empcode]" value="<?php echo  $user["empcode"];?>">
                                    
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="user[email]">Email</label>
                            <div class="controls">
                                <input type="text"  name="user[email]" value="<?php echo  $user["email"];?>">
                                    
                            </div>
                        </div> 
                            <div class="control-group">
                            <label class="control-label" for="cpass">User Type</label>
                            <div class="controls"> 
                                <select name="user[emptype]">
                                    <option value="0" <?php   if($user["emptype"]==0){?> selected <?php } ?> >Select</option>
                                    <option value="2" <?php   if($user["emptype"]==2){?> selected <?php } ?>>Developer</option>
                                    <option value="3" <?php   if($user["emptype"]==3){?> selected <?php } ?>>Project Manager</option>
                                </select>
                            </div>
                        </div> 
                        </div> 
                            <div class="form-actions">
                                <input type="hidden" value="<?php echo $user["id"]; ?>" name="uid">
                                <input type="hidden" value="<?php echo $user["email"]; ?>" name="email_old">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </fieldset>
              </form>
              </div>
        </div>
    </div>