<div id="content" class="span10">
    <div class="row-fluid sortable">
       
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-book"></i> Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
              <div class="box-content">
              <form class="form-horizontal" method="post" action="<?php echo site_url('user/save'); ?>">
                    <fieldset>
                        <legend>Add New User</legend>
                        <?php if(isset($error_msg)){?>
                         <div class="alert alert-info">
                        <?php echo $error_msg; ?>
                         </div>
                        <?php } ?>
                        <?php if(isset($_SESSION["success_msg"])){?>
                         <div class="alert alert-success">
                        <?php 
                        $msg=$_SESSION["success_msg"];
                        echo $msg;
                        unset($_SESSION["success_msg"]);
                        ?>
                         </div>
                        <?php } ?>
                         <div class="control-group">
                            <label class="control-label" for="user[first_name]">First Name</label>
                            <div class="controls">
                                <input type="text" name="user[first_name]" value="<?php echo  $_SESSION["reguser_first_name"];?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="user[last_name]">Last Name</label>
                            <div class="controls">
                                <input type="text" name="user[last_name]" value="<?php echo  $_SESSION["reguser_last_name"];?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="user[empcode]">Employee Code</label>
                            <div class="controls">
                                <input type="text"  name="user[empcode]" value="<?php echo  $_SESSION["reguser_empcode"];?>">
                                    
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="user[email]">Email</label>
                            <div class="controls">
                                <input type="text"  name="user[email]" value="<?php echo  $_SESSION["reguser_email"];?>">
                                    
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="user[password]">Password</label>
                            <div class="controls">
                                <input type="text"  name="user[password]">
                                    
                            </div>
                            </div> 
                            <div class="control-group">
                            <label class="control-label" for="cpass">Confirm Password</label>
                            <div class="controls">
                                <input type="text"  name="cpass">
                                    
                            </div>
                            </div> 
                            <div class="control-group">
                            <label class="control-label" for="cpass">User Type</label>
                            <div class="controls"> 
                                <select name="user[emptype]">
                                    <option value="0" <?php   if($_SESSION["reguser_emptype"]==0){?> selected <?php } ?> >Select</option>
                                    <option value="2" <?php   if($_SESSION["reguser_emptype"]==2){?> selected <?php } ?>>Developer</option>
                                    <option value="3" <?php   if($_SESSION["reguser_emptype"]==3){?> selected <?php } ?>>Project Manager</option>
                                </select>
                            </div>
                        </div> 
                        </div> 
                            <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Register</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </fieldset>
              </form>
              </div>
        </div>
    </div>
</div>