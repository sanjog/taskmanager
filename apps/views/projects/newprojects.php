<div id="content" class="span10">
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-edit"></i> Add New Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                    
                <form class="form-horizontal" method="post" action="<?php echo site_url('projects/save'); ?>">
                    <fieldset>
                        <legend>Add New Project</legend>
                <div class="box-content span5">
                      
                        <div class="control-group">
                            <label class="control-label" for="projetctname[]">Project Name</label>
                            <div class="controls">
                                <input type="text" required="required" name="data[project_name]">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Project URL</label>
                            <div class="controls">
                                <input type="text" required="required" name="data[project_url]">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Client Code</label>
                            <div class="controls">
                                <input type="text"  name="data[client_code]">
                                    
                            </div>
                        </div> 
                
                        <div class="control-group">
                            <label class="control-label" for="fileInput">Project Plan</label>
                            <div class="controls">
                                <select  name="data[project_plan]">
                                    <?php foreach($projectplans as $key=>$projectplan){?>
                                    <option value="<?php echo $projectplan['id']?>"><?php echo $projectplan['project_plan'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                     <div class="control-group">
                            <label class="control-label" for="fileInput">Status</label>
                            <div class="controls">
                                <select  name="data[status]">
                                    <option value="1">NOT YET STARTED</option>
                                    <option value="2">PROGRESS</option>
                                    <option value="3">NEED MORE INFO</option>
                                    <option value="4">PAUSED</option>
                                    <option value="5">DONE</option>
                                </select>
                            </div>
                        </div> 
                        </div>
                         <div class="box-content span5">
                         <div class="control-group">
                            <label class="control-label" for="fileInput">FTP Login Details</label>
                            <div class="controls">
                                <textarea  name="data[ftp_info]"></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Control Panel Login Details</label>
                            <div class="controls">
                                <textarea  name="data[controlpanel_info]"></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">CMS Login Details</label>
                            <div class="controls">
                                <textarea  name="data[cms_info]"></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Other Login Details</label>
                            <div class="controls">
                                <textarea  name="data[other_info]"></textarea>
                            </div>
                        </div> 
                        
                         </div>
                        <div class="row-fluid span12">
                        <div class="control-group">
                            <label class="control-label" for="textarea2">Description</label>
                            <div class="controls">
                                <textarea class="cleditor" required="required" name="data[project_description]" id="textarea2" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">ADD Project</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/#content.span10-->