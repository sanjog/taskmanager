<div id="content" class="span10">
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-edit"></i> Add New Projects</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                    
                <form class="form-horizontal" method="post" action="<?php echo site_url('projects/update'); ?>">
                    <fieldset>
                        <legend>Edit Project,<?php echo $projects["project_name"]; ?></legend>
                <div class="box-content span5">
                      
                        <div class="control-group">
                            <label class="control-label" for="projetctname[]">Project Name</label>
                            <div class="controls">
                                <input type="text" name="data[project_name]" required="required" value="<?php echo $projects["project_name"]; ?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Project URL</label>
                            <div class="controls">
                                <input type="text" name="data[project_url]" required="required" value="<?php echo $projects["project_url"]; ?>">
                                    
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Client Code</label>
                            <div class="controls">
                                <input type="text"  name="data[client_code]" value="<?php echo $projects["client_code"]; ?>" >
                                    
                            </div>
                        </div> 
                
                        <div class="control-group">
                            <label class="control-label" for="fileInput">Project Plan</label>
                            <div class="controls">
                                <select  name="data[project_plan]">
                                
                                    <?php foreach($projectplans as $key=>$projectplan){?>
                                    <option value="<?php echo $projectplan['id']?>" <?php if($projects["project_plan"] == $projectplan['id'] ){ ?> selected <?php } ?> ><?php echo $projectplan['project_plan'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                     <div class="control-group">
                            <label class="control-label" for="fileInput">Status</label>
                            <div class="controls">
                                <select  name="data[status]">
                                    <option value="1" <?php if($projects["status"] == 1 ){ ?> selected <?php } ?> >NOT YET STARTED</option>
                                    <option value="2" <?php if($projects["status"] == 2 ){ ?> selected <?php } ?>>PROGRESS</option>
                                    <option value="3" <?php if($projects["status"] == 3 ){ ?> selected <?php } ?> >NEED MORE INFO</option>
                                    <option value="4" <?php if($projects["status"] == 4 ){ ?> selected <?php } ?> >PAUSED</option>
                                    <option value="5" <?php if($projects["status"] == 5 ){ ?> selected <?php } ?> >DONE</option>
                                </select>
                            </div>
                        </div> 
                        </div>
                         <div class="box-content span5">
                         <div class="control-group">
                            <label class="control-label" for="fileInput">FTP Login Details</label>
                            <div class="controls">
                                <textarea  name="data[ftp_info]" ><?php echo $projects["ftp_info"]; ?></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Control Panel Login Details</label>
                            <div class="controls">
                                <textarea  name="data[controlpanel_info]"><?php echo $projects["controlpanel_info"]; ?></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">CMS Login Details</label>
                            <div class="controls">
                                <textarea  name="data[cms_info]"><?php echo $projects["cms_info"]; ?></textarea>
                            </div>
                        </div> 
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Other Login Details</label>
                            <div class="controls">
                                <textarea  name="data[other_info]"><?php echo $projects["other_info"]; ?></textarea>
                            </div>
                        </div> 
                        
                         </div>
                        <div class="row-fluid span12">
                        <div class="control-group">
                            <label class="control-label" for="textarea2">Description</label>
                            <div class="controls">
                                <textarea class="cleditor" required="required" name="data[project_description]" id="textarea2" rows="3"><?php echo $projects["project_description"]; ?></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="pid" value="<?php echo $projects["id"]; ?>"/>
                            <button type="submit" class="btn btn-primary">UPDATE Project</button>
                        </div>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

</div><!--/#content.span10-->
