<script type="text/javascript">
    $(document).ready(function() {




        loadComments();
        $("#givecomment").click(function() {

            $("#comment-loader").show("slow");
            $("#givecomment").attr("disabled", "disabled");
             var str = $("#comments").val();
                var regex = /<br\s*[\/]?>/gi;
                $("#comments").val(str.replace(/\n/g, "<br />"));
            var data = $("#dataform").serialize();
            $.post("<?php echo site_url('comments/saveComments') ?>", data, function(response) {
                $("#comment-loader").hide("slow");
                $("#comments").val("");
                $("#givecomment").removeAttr("disabled");
                loadComments();

            });

        });
        $("#comments").keydown(function(e) {
            if (e.keyCode === 10 || e.keyCode == 13 && e.ctrlKey) {
                $(this).val(function(i, val) {
                    return val + '\n';
                });
            }
        });
        

    });
    function remove_data(obj){
    var status = confirm("Do want to Detele");
        if(status){
            
              var data1 = "comment_id=" +obj;
        $.post("<?php echo site_url('comments/deleteComments') ?>", data1, function(response) {
        if(response==1){
               $("#remove"+obj).hide("slow");
               loadComments();
            }else{
                alert("Time Exceeded (5 min)");
            }
            });
        }else{
        
        }
       
    }
    function loadComments() {
        var data1 = "project_id=" + $("#projectid").val();
        $.post("<?php echo site_url('comments/getComments') ?>", data1, function(response) {

            var remove='';
            var htmlresponse = "";
            var x = 0;
            var counter = 0;
            var htmldata = "";
            var arr = new Array();
            $.each(response, function(key, value) {
                $.each(value, function(key1, value1) {

                    arr[key1] = value1;
                    htmlresponse += key1 + '= ' + value1 + " || ";
                    x++;
                });
                var t = arr["posted_time"].split(/[- :]/);
                var date = new Date(t[0], t[1] - 1, t[2], t[3], t[4]);
                var userid=<?php echo $_SESSION['user_id']; ?>;
                if(userid==arr['user_id']){
                   remove="<span onclick='remove_data("+arr['id']+")' style='cursor: pointer' class='icon-trash'></span>";
                }else{
                    remove="";
                }
                htmldata += "<section id='remove"+arr['id']+"'><h4>" + arr['first_name'] + " " + arr["last_name"] + " posted on"+ remove +"</h4><h6>" + date + "</h6><p style='word-wrap: break-word;background:#FFFBF2'>" + arr["comments"] + "</p><hr></section>";
                counter++;
                htmlresponse += "<br>";
            });


            $("#comments-area").html(htmldata);
            $("#comment-head").html("Comments(" + counter + ")");
        }, 'json');
    }
</script>


<div id="content" class="span10">
    <div class="span12">
    <div class="row-fluid ">
        
        <div class="span8">
 <div class="row-fluid ">
            <div class="box span6">
            <div class="box-header well" data-original-title>
                <h3>General Info</h3>
            </div>
            <div class="box-content">
                <dl>
                    <dt>Project Name</dt>
                    <dd><?php echo $project['project_name']; ?></dd>
                    <dt>Project URL</dt>
                    <dd><?php echo $project['project_url']; ?></dd>
                    <dt>Client Code</dt>
                    <dd><?php echo $project['client_code']; ?></dd>
                    <dt>Create Date</dt>
                    <dd><?php echo date_readable($project['date_created']); ?></dd>
                    <dt>Assigned By</dt>
                    <dd><?php echo $project['first_name'] . " " . $project['last_name']; ?></dd>
                    <dt>Project Plan</dt>
                    <dd><?php echo $project['project_plan']; ?></dd>
                    <dt>Project Status</dt>
                    <dd>
                        <?php if ($project['status'] == 1) { ?>
                            <span class="label label-info">
                                Not yet started
                            </span>
                        <?php } ?>
                        <?php if ($project['status'] == 2) { ?>
                            <span class="label label-important">
                                Progress
                            </span>
                        <?php } ?>
                        <?php if ($project['status'] == 3) { ?>
                            <span class="label label-inverse">
                                Need more info
                            </span>
                        <?php } ?>
                        <?php if ($project['status'] == 4) { ?>
                            <span class="label label-warning">
                                Paused
                            </span>
                        <?php } ?>
                        <?php if ($project['status'] == 5) { ?>
                            <span class="label label-success">
                                Done
                            </span>
                        <?php } ?>
                    </dd>
                </dl>            
            </div>
        </div><!--/span-->

        <div class="box span6">
            <div class="box-header well" data-original-title>
                <h3>Login Info</h3>

            </div>
            <div class="box-content">
                <dl>
                    <dt>FTP Login Info</dt>
                    <dd><?php echo $project['ftp_info']; ?></dd>
                    <dt>Control Panel Info</dt>
                    <dd><?php echo $project['controlpanel_info']; ?></dd>
                    <dt>CMS Info</dt>
                    <dd><?php echo $project['cms_info']; ?></dd>
                    <dt>Other Info</dt>
                    <dd><?php echo $project['other_info']; ?></dd>

                </dl>            
            </div>
        </div><!--/span-->
          <div class="row-fluid ">  
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h3>Description List</h3>

            </div>
            <div class="box-content">
                <dl>
                    <dt>Description</dt>
                    <dd><?php echo $project['project_description']; ?></dd>
                    </dl>            
            </div>
        </div><!--/span-->
    </div>
        </div>
            
    </div>
        <div class="span4">
        <div class="box span12 ">
            <div class="box-header well" data-original-title>
                <h3>Project Files</h3>

            </div>
            <div class="box-content">
                <div>
                    <form method="post" enctype="multipart/form-data"  action="<?php echo site_url("upload/doUpload");?>">
            <input type="file" name="images" id="files" />
                <input type="hidden" name="pid" id="pid" value="<?php echo $pid; ?>" />
            <button type="submit" id="btn">Upload Files!</button>
        </form>
                </div>
                <div id="response"></div>
                <dl >
                  
                    <dt id="file-list">Project Files</dt>
                    <dd></dd>
                   
                    
                </dl>            
            </div>
        </div><!--/span-->
            </div>

    </div><!--/row-->
</div>
    <hr>
   
    <h3 id="comment-head">Comments</h3>
    <hr>

    <div class="row-fluid " id="comments-area"> 

        no comments to display
    </div>
    <hr>
    <div class="row-fluid ">        
        <div class="row-fluid span12 ">
            <form id="dataform">
                <div class="control-group ">
                    <label class="control-label" for="textarea2">Comments</label>
                    <div class="controls">
                        <textarea class="span10"  name="comments" id="comments" rows="5"></textarea>
                        <input type="hidden" name="project_id" id="projectid" value="<?php echo $pid; ?>">

                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="givecomment" class="btn btn-primary">Comment</button>
                    <button type="reset" class="btn">Cancel</button>
                    <img id="comment-loader" style="display: none" src="<?php echo assets_url(); ?>img/ajax-loaders/comments.gif" >
                </div>
            </form>
        </div>
    </div>
</div><!--/#content.span10-->

<script type="text/javascript">
    $("document").ready(function () {
        var data="pid="+$("#pid").val();
    $.post("<?php echo site_url("upload/listFiles");?>",data,function(response){
        $("#file-list").html(response)
    });
    var input = document.getElementById("files"), 
    pid = document.getElementById("pid").value,
        formdata = false;
    if (window.FormData) {
        formdata = new FormData();
        $("#btn").hide();
    }
    
    input.addEventListener("change", function (evt) {
        document.getElementById("response").innerHTML = "Uploading . . ."
        var i = 0, len = this.files.length, img, reader, file;
    
        for ( ; i < len; i++ ) {
            file = this.files[i];
                var ext=file.name.split('.').pop();
            if ($.inArray(ext, ['gif','png','jpg','jpeg','pdf','doc','docx','xls','xlsx','odt','ods','ppt','pptx','txt','html','xml','psd']) != -1) {
                if (formdata) {
                    formdata.append("files[]", file);
                                        formdata.append("pid",pid);
                    $.ajax({
                url: "<?php echo site_url("upload/doUpload");?>",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    $("#response").html(res); 
                     showUploadedItem();
                }
            });
                }
            }else{
                document.getElementById("response").innerHTML = ""
                alert("invalid file type");
            }   
        }
    }, false);
}());
function showUploadedItem () {
                var data="pid="+$("#pid").val();
        $.post("<?php echo site_url("upload/listFiles");?>",data,function(response){
        $("#file-list").html(response)
    });
    }  
</script>
