<div id="content" class="span10">



    <div class="row-fluid sortable">		
        <div class="box span12">

            <div class="box-header well" data-original-title>
                <h2><i class="icon-book"></i> Projects</h2>
                <div class="box-icon">
                    <a href="#addform" class="btn btn-setting btn-round fancy-box"><i class="icon-plus"></i></a>
                    <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sl</th>
                            <th>Project Plan</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        
                        <?php foreach($projectplans as $key=>$projectplan){?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td><?php echo $projectplan['project_plan'];?></td>
                            <td class="center">
                                <a class="btn btn-success" href="#">
                                    <i class="icon-zoom-in icon-white"></i>  
                                    View                                            
                                </a>
                                <a class="btn btn-info" href="#">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a>
                                <a class="btn btn-danger" href="#">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->
</div><!--/#content.span10-->
<form id="addform" style="display: none" method="post" action="<?php echo site_url('project_plan/save'); ?>">
                    <div class="control-group">
                        <label class="control-label" for="projectplan">New Project Plan</label>
                        <div class="controls">
                            <input type="text" name="projectplan">

                        </div>
                    </div> 
                    <div >
                        <button type="submit" class="btn">Add</button>
                    </div>
                </form>