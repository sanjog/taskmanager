		
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="<?php echo site_url('secure_area/index');?>"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
                                                <?php if($_SESSION["emptype"]==1){?> <li><a href="<?php echo site_url('project_plan/index'); ?>"><i class="icon-lock"></i><span class="hidden-tablet">Manage Project Plan</span></a></li><?php } ?>
						<?php if($_SESSION["emptype"]==3 ||  $_SESSION["emptype"]==1  ){?> <li><a href="<?php echo site_url('projects/index');?>"><i class="icon-lock"></i><span class="hidden-tablet">Add New Project</span></a></li><?php } ?>
                                               <?php if($_SESSION["emptype"]==1){?> <li><a href="<?php echo site_url('user/retrive'); ?>"><i class="icon-lock"></i><span class="hidden-tablet">Manage User</span></a></li><?php } ?>
 <?php if($_SESSION["emptype"]==1){?> <li><a href="<?php echo site_url('user/registrationForm'); ?>"><i class="icon-lock"></i><span class="hidden-tablet">Add User</span></a></li><?php } ?>
                                                
					</ul>
					<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
