<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>Task Manager</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">
	<script src="<?php echo assets_url(); ?>js/jquery-1.7.2.min.js"></script>
	<!-- The styles -->
	<link id="bs-css" href="<?php echo assets_url(); ?>css/bootstrap-journal.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo assets_url(); ?>fancybox/jquery.fancybox.css">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
 .box-content {
padding: 10px;
word-wrap: break-word;
}
	</style>
	<link href="<?php echo assets_url(); ?>css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<?php echo assets_url(); ?>css/charisma-app.css" rel="stylesheet">
	<link href="<?php echo assets_url(); ?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='<?php echo assets_url(); ?>css/fullcalendar.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='<?php echo assets_url(); ?>css/chosen.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/uniform.default.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/colorbox.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/jquery.cleditor.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/jquery.noty.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/noty_theme_default.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/elfinder.min.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/elfinder.theme.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/opa-icons.css' rel='stylesheet'>
	<link href='<?php echo assets_url(); ?>css/uploadify.css' rel='stylesheet'>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
		
</head>

<body>
<div style="position:absolute;top:10px;left:30%;width:30%;height:auto;background-color: #124dfd;color:#ffffff;opacity:0.3;text-allign:center;font-size:large;z-index:99; border-radius:4px; padding-top:10px; padding-bottom:10px">


<?php
echo "<center>";
echo "Hello ".$_SESSION["first_name"]."  ".$_SESSION["last_name"].","; 
if($_SESSION["emptype"]==1){
echo "Admin";
}
if($_SESSION["emptype"]==2){
echo "Developer";
}
if($_SESSION["emptype"]==3){
echo "Project Manager";
}
echo "</center>";
?>
</div>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="" href="/"> <img src="<?php echo assets_url(); ?>/img/logo.png" /></a>
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"><?php echo $_SESSION["first_name"];?></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
                                                <li><a href="<?php echo site_url('login/logout'); ?>">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
		
	
