<?php
class Download extends Controller{
    function __construct() {
        parent::__construct();
    }
    function doDownload(){
       $pid = $_GET["pid"];
       $filename=$_GET["filename"];
	$filetype=explode(".",$filename);
	$reversed = array_reverse($filetype);
       $file = "assets/uploads/project_file_".$pid."/".$filename;
       $title = $filename;
	$mime = shell_exec("file -bi " . $file);
    header("Pragma: public");
    header('Content-disposition: attachment; filename='.$title);
    header("Content-type: ".$mime);
    header("Content-Length: " . filesize($file) ."; ");
    header('Content-Transfer-Encoding: binary');
    ob_clean();
    flush();

    $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
    if (filesize($file) > $chunksize) {
        $handle = fopen($file, 'rb');
        $buffer = '';

        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            echo $buffer;
            ob_flush();
            flush();
        }

        fclose($handle);
    } else {
        readfile($file);
    }
    }
}
