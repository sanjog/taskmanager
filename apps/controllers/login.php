<?php

class Login extends Controller {

    function __construct() {
        parent::__construct();
        $this->load_model("login");
    }

    function index() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
           $email=$_POST['email'];
           $password=$_POST['password'];
           $status=$this->login->doLogin($email,$password);
           if($status==1){
               $this->redirect('secure_area/index');
           }else{
               $this->load_view("login/login");
           }
        }else{
        $this->load_view("login/login");
    }
    }
    function logout(){
        session_destroy();
        $this->redirect('login/index');
    }
            function register() {
        
    }

}
