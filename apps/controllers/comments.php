<?php
class Comments extends Controller{
    function __construct() {
        parent::__construct();
        $this->load_model("comments");
        $this->load_model("projects");
         $this->load_model("login");
        if(!$this->login->is_login()){
            echo "not logged in ";
            exit;
        }
    }
    function getComments(){
        
        $data=$this->comments->getComments($_POST['project_id']);
        echo json_encode($data);
        
    }
    function saveComments(){
        
       $data['comments']=$_POST['comments'];
       $data['user_id'] = $_SESSION['user_id'];
       $data['project_id']=$_POST['project_id'];
       $data['ip']=$_SERVER['REMOTE_ADDR'];
       $data['status']=1;
       $this->comments->saveComments($data);
        $project=$this->projects->retriveProjectById($_POST['project_id']);
      
         $body=$_SESSION['first_name']." ".$_SESSION['last_name']." commented on project ".$project["project_name"];
        $heading="Comment Notification";
       
        $email= $this->projects->getEmails($project["assigned_by"]);
        $this->sendMail($heading,$body,$email);  
    }
    function deleteComments(){
        $id=$_POST['comment_id'];
        
        $status=$this->comments->deleteComments($id);
        echo $status;
    }
}
