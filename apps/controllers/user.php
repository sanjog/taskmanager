<?php

class User extends Controller {

    function __construct() {
        parent::__construct();

        $this->load_model("user");
         $this->load_model("login");
        if(!$this->login->is_login()){
            $this->redirect('login/index');
        }
    }

    function registrationForm() {
        $data['request_page'] = 'user/registration';
        $this->load_view('common/common', $data);
    }

    function save() {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $error_msg = '';
            $error_status = 1;
            $data = $_POST['user'];
            $password=$data["password"];
            $data["password"] = md5($data["password"]);
            $data["status"] = 1;
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $cpass = $_POST["cpass"];
            if (md5($cpass) != $data["password"]) {
                $error_msg .="Password Mismatched<br>";
                $error_status = 0;
            }
            if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
                $error_msg .="Not a valid Email<br>";
                $error_status = 0;
            } else {
                if ($this->user->checkEmail($data["email"]) == 1) {
                    $error_msg .="Email already in use<br>";
                    $error_status = 0;
                }
            }

            if ($data["first_name"] == '') {
                $error_msg .="First name is empty<br>";
                $error_status = 0;
            }
            if ($data["last_name"] == '') {
                $error_msg .="Last name is empty<br>";
                $error_status = 0;
            }
            if ($data["empcode"] == '') {
                $error_msg .="Employee Code is empty<br>";
                $error_status = 0;
            }
            if ($data["emptype"] == 0) {
                $error_msg .="Employee type is not selected<br>";
                $error_status = 0;
            }

            if ($error_status == 0) {
                $data['request_page'] = 'user/registration';
                $_SESSION["reguser_first_name"] = $data["first_name"];
                $_SESSION["reguser_last_name"] = $data["last_name"];
                $_SESSION["reguser_email"] = $data["email"];
                $_SESSION["reguser_empcode"] = $data["empcode"];
                $_SESSION["reguser_emptype"] = $data["emptype"];
                $data['request_page'] = 'user/registration';
                $data["error_msg"] = $error_msg;
                $this->load_view('common/common', $data);
            } else {
                unset($_SESSION["reguser_first_name"]);
                unset($_SESSION["reguser_last_name"]);
                unset($_SESSION["reguser_email"]);
                unset($_SESSION["reguser_empcode"]);
                unset($_SESSION["reguser_emptype"]);
                $_SESSION["success_msg"] = "User Added Successfully";
                $this->user->insertdata($data);
                $message="Congratulation ".$data["first_name"]." ".$data["last_name"].",<br> Your account has been created<br><br>Your Login Details <br><br>email : <b>".$data["email"]."</b>  <br><br>  password: <b>".$password."</b> <br><br> Login URL: <b>".$this->site_url("login/index")."</b>";
             $heading="New Account Info";
             $email[0]["email"]=$data["email"];
             $email[0]["first_name"]=$data["first_name"];
             $email[0]["last_name"]=$data["last_name"];
             $this->sendMail($heading,$message,$email);
                $this->redirect('user/registrationForm');
            }
        }
    }

   function trashProject() {
        $id = $_GET["uid"];
        $data["status"] = 2;
        $this->user->trashProject($data, $id);
        $this->redirect('user/retrive');
    }

    function edit() {
        $uid = $_GET["uid"];
        $user = $this->user->retriveUserById($uid);
        $data["user"] = $user[0];
        $data['request_page'] = 'user/edituser';
        $this->load_view('common/common', $data);
    }

    function update() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $uid = $_POST["uid"];
            $email_old = $_POST["email_old"];
            $error_msg = '';
            $error_status = 1;
            $data = $_POST['user'];
            $data["status"] = 1;
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $cpass = $_POST["cpass"];

            if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
                $error_msg .="Not a valid Email<br>";
                $error_status = 0;
            } else {
                if ($data["email"] == $email_old) {
                    
                } else {
                    if ($this->user->checkEmail($data["email"]) == 1) {
                        $error_msg .="Email already in use<br>";
                        $error_status = 0;
                    }
                }
            }
            
            if ($data["first_name"] == '') {
                $error_msg .="First name is empty<br>";
                $error_status = 0;
            }
            if ($data["last_name"] == '') {
                $error_msg .="Last name is empty<br>";
                $error_status = 0;
            }
            if ($data["empcode"] == '') {
                $error_msg .="Employee Code is empty<br>";
                $error_status = 0;
            }
            if ($data["emptype"] == 0) {
                $error_msg .="Employee type is not selected<br>";
                $error_status = 0;
            }

            if ($error_status == 0) {
                $_SESSION["error_msg"] = $error_msg;
                $this->redirect("user/edit&uid=$uid");
            } else {
                $_SESSION["success_msg"] = "User updated Successfully";
                $this->user->updatedata($data,$uid);
                $this->redirect("user/retrive");
            }
        }
    }

    function retrive() {
        $data["users"] = $this->user->retrive();
        $data['request_page'] = 'user/manageuser';
        $this->load_view('common/common', $data);
    }

}