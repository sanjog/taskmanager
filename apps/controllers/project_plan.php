<?php

class Project_plan extends Controller{
    function __construct() {
        parent::__construct();
        $this->load_model('project_plan');
    }
    function index(){
          $data['projectplans']=$this->project_plan->retrivedata();
          $data['request_page']='projectplan/manage';
        $this->load_view('common/common',$data);
    }
    function save(){
          if ($_SERVER['REQUEST_METHOD'] === 'POST') {
              $data['project_plan']=$_POST['projectplan'];
              $this->project_plan->insertdata($data);
              $this->redirect('project_plan/index');
          }
    }
}