<?php



class Projects extends Controller {

    function __construct() {

        parent::__construct();
        ini_set("display_error", 0);
        $this->load_model('project_plan');
         $this->load_model("login");
        $this->load_model('projects');
        $this->load_model('edit_log');
        if($this->login->is_login()==0){
            $this->redirect('login/index');
        }
        
    }

    function index() {
        if($_SESSION["emptype"]==3 || $_SESSION["emptype"]==1 ){
        $data['projectplans'] = $this->project_plan->retrivedata();
        $data['request_page'] = 'projects/newprojects';
        $this->load_view('common/common', $data);
    }else{
        $this->redirect("secure_area/index");
    }
    }

    function save() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = $_POST['data'];
            $data['assigned_by'] = $_SESSION['user_id'];
            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $last_id=$this->projects->insertdata($data);
             $heading="New Project Added";
             $message=$_SESSION["first_name"]." ".$_SESSION["last_name"]." just added a new project <a href='".$this->site_url("projects/viewProject&pid=$last_id")."'> <b>".$data["project_name"]."</b></a>";
             $email= $this->projects->getEmails($_SESSION['user_id']);
             $this->sendMail($heading,$message,$email);
            $this->redirect('projects/index');
        }
    }

    function update() {
        $count = 0;
        $message = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = $_POST['data'];
            $id = $_POST['pid'];

            $data['ip'] = $_SERVER['REMOTE_ADDR'];
            $log["project_id"] = $id;
            $log["user_id"] = $_SESSION["user_id"];
            $log['ip'] = $_SERVER['REMOTE_ADDR'];
            $project = $this->projects->retriveProject($id);
            $old_project = $project[0];
            $this->projects->updatedata($data, $id);
            $this->edit_log->insertLog($log);
            $project1 = $this->projects->retriveProject($id);
            $new_project = $project1[0];
            foreach ($new_project as $key => $value) {
                if (strcmp($new_project[$key], $old_project[$key]) == 0) {
                    
                } else {
                    $result[$key] = $value;
                }
            }
            foreach ($result as $key => $value) {
                $count++;
                if ($count % 2 == 0) {
                    unset($result[$key]);
                }
            }
            $message .= "
                <div>
                <p>Project Name:: <b><a style='cursor:pointer;' href='".$this->site_url("projects/viewProject&pid=$id")."'>".$old_project["project_name"]."</a></b></p>
                </div>
                <table border='2'>
                <tr>
                <th>Field Name</th>
                <th>New Value</th>
                <th> Old Value</th>
                </tr>";

            foreach ($result as $key => $value) {
                $message .= "<tr>";
                $message .= "<td>" . $key . "</td>";
                if ($key == "status") {
                    if ($value == 1) {
                        $message .= "<td>NOT YET STARTED</td>";
                    }
                    if ($value == 2) {
                        $message .= "<td>PROGRESS</td>";
                    }
                    if ($value == 3) {
                        $message .= "<td>NEED MORE TIME</td>";
                    }
                    if ($value == 4) {
                        $message .= "<td>PAUSED</td>";
                    }
                    if ($value == 5) {
                        $message .= "<td>DONE</td>";
                    }

                    if ($old_project[$key] == 1) {
                        $message .= "<td>NOT YET STARTED</td>";
                    }
                    if ($old_project[$key] == 2) {
                        $message .= "<td>PROGRESS</td>";
                    }
                    if ($old_project[$key] == 3) {
                        $message .= "<td>NEED MORE TIME</td>";
                    }
                    if ($old_project[$key] == 4) {
                        $message .= "<td>PAUSED</td>";
                    }
                    if ($old_project[$key] == 5) {
                        $message .= "<td>DONE</td>";
                    }
                } else if ($key == "project_plan") {
                    $message .= "<td>" . $this->project_plan->nameById($value) . "</td>";
                    $message .= "<td>" . $this->project_plan->nameById($old_project[$key]) . "</td>";
                } else {
                    $message .= "<td>" . $value . "</td>";
                    $message .= "<td>" . $old_project[$key] . "</td>";
                }

                $message .= "</tr>";
            }
            $message .= "</table>";

           $email= $this->projects->getEmails($old_project["assigned_by"]);
            $heading="Update Notification";
        $this->sendMail($heading,$message,$email);  
          $this->redirect('secure_area/index');
        }
    }

    function viewProject() {
        $id = $_GET['pid'];
        $data["pid"] = $id;
        $data['project'] = $this->projects->retriveProjectById($id);
        $data['request_page'] = 'projects/viewproject';
        $this->load_view('common/common', $data);
    }

    function jsontest() {
        echo json_encode($this->project_plan->retrivedata());
    }

    function editProject() {
        $id = $_GET["pid"];
        $data['projectplans'] = $this->project_plan->retrivedata();
        $project = $this->projects->retriveProject($id);
        $data["projects"] = $project[0];
        if($_SESSION['emptype']==2 || $_SESSION['emptype']==3 || $_SESSION['emptype']==1){
        if($project[0]['assigned_by']==$_SESSION['user_id'] || $_SESSION['emptype']==2 || $_SESSION['emptype']==1 ){
         $data['request_page'] = 'projects/editprojects';
              $this->load_view('common/common', $data); 
        }else{
            $this->redirect('secure_area/index');  
        }
        }else{
          $this->redirect('secure_area/index');  
        }
       
       
    }

    function trashProject() {
        
        $id = $_GET["pid"];
        $project = $this->projects->retriveProject($id);
       
        if($_SESSION['emptype']==3 || $_SESSION['emptype']==1){
        if($project[0]['assigned_by']==$_SESSION['user_id'] || $_SESSION['emptype']==1 ){
            $data["delete_status"] = 2;
            $this->projects->trashProject($data, $id);
            $heading="Project Trashed";
             $message=$_SESSION["first_name"]." ".$_SESSION["last_name"]." trashed the project  project <b>".$project[0]['project_name']."</b>";
             $email= $this->projects->getEmails($project[0]['assigned_by']);
             $this->sendMail($heading,$message,$email);
            $this->redirect('secure_area/index'); 
        }else{
           $this->redirect('secure_area/index');  
        }
        }else{
           $this->redirect('secure_area/index'); 
        }
        
    }


}
