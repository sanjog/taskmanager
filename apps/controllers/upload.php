<?php

class Upload extends Controller {

    function __construct() {
        parent::__construct();
        set_time_limit (300);
        $this->load_model("projects");
         $this->load_model("login");
        if(!$this->login->is_login()){
            echo "not logged in ";
            exit;
        }
    }

    function doUpload() {
        $dirname = "project_file_".$_POST['pid'];
        $dirname = "assets/uploads/" . $dirname . "/";
        if (!file_exists($dirname)) {
            mkdir($dirname, 0777);
        } 
        $status_msg = " ";
        $filelength = sizeof($_FILES["files"]["error"]);
//echo "<pre>";
//print_r($_FILES);
//echo "</pre>";
        foreach ($_FILES["files"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $temp_file_name = $_FILES["files"]["name"][$key];
                $name=str_replace(" ","_",$temp_file_name);
                if ($filelength - 1 == $key) {
                    if (file_exists($dirname . $name)) {
                        $prefix=1;
                        $check=1;
                        $filename="copy($prefix)_".$name;
                        while($check){
                           
                             $prefix++;
                            $filename="copy($prefix)_".$name;
                       
                        if(file_exists($dirname.$filename)){
                        
                        }else{
                            $check=0;
                            $status_msg .="$filename Uploaded successfully<br>";
                           move_uploaded_file($_FILES["files"]["tmp_name"][$key], $dirname .$filename);
                        }
                        }
                        
                    } else {
                        $status_msg .="Uploaded successfully<br>";
                        $filename=$name;
                        move_uploaded_file($_FILES["files"]["tmp_name"][$key], $dirname . $name);
                    }
                } else {
                    if (file_exists($dirname . $name)) {
                        
                    } else {
                        $filename=$name;
                        move_uploaded_file($_FILES["files"]["tmp_name"][$key], $dirname . $name);
                    }
                }
            }
        }
        $project=$this->projects->retriveProjectById($_POST['pid']);
        $body=$_SESSION['first_name']." ".$_SESSION['last_name']." just upload a file $filename on project ".$project["project_name"]."<br><a target='_blank' href='".$this->site_url("projects/viewproject")."&pid=".$_POST['pid']."'>Project URL</a>";
        $heading="File Upload Notification";
      
        $email= $this->projects->getEmails($project["assigned_by"]);
	echo "<h4>" . $status_msg . "</h4>";
        $this->sendMail($heading,$body,$email); 
   
        
        
    }

    function listFiles() {
        $dir="assets/uploads/project_file_".$_POST["pid"]."/";
        foreach (glob("$dir*.*") as $filename) {
             echo "<li><a href='".$this->site_url("download/doDownload")."&pid=".$_POST["pid"]."&filename=" . str_replace($dir, "", $filename) . "'>" . str_replace($dir, "", $filename) . "</a></li>";
        }
    }

}
