<?php
class Secure_area extends Controller{
    function __construct() {
        parent::__construct();
        
        $this->load_model('projects');
        $this->load_model("login");
        if(!$this->login->is_login()){
            $this->redirect('login/index');
        }
    }
    function index(){
            $data['projects']=  $this->projects->retrivedata();
        $data['request_page']='secure_area/dashboard';
        $this->load_view('common/common',$data);
        
    }
}