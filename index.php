<?php

/*
 *----------------------------------------------------------------------------------
 * TASK MANAGER :: BASED ON CUSTOM MADE MVC FRAMEWORK DESIGNED BY SANJOG KUMAR DASH
 *----------------------------------------------------------------------------------
 *
 * 
 * 
 *  PHP Taskmanager is a simple TODO style task manager application with different access control.

 * Multi User option
 * Easy to use boostrap based UI
 * Multiple project option
 * For login use Username as "sanjog152@gmail.com" And password "admin"
 *
 *  Version
 *  3.0.2
 *
 *
 *
 *
 */

session_start();
$app_path="apps/";
$base_path="core/";
define("APPPATH",$app_path);
define("BASEPATH", $base_path);
require_once BASEPATH.'root/route.php';
require_once BASEPATH.'root/autoload.php';

?>