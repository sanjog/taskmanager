-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 05, 2014 at 02:16 PM
-- Server version: 5.5.38
-- PHP Version: 5.4.33-2+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `comments` text NOT NULL,
  `posted_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `project_id`, `comments`, `posted_time`, `status`, `ip`) VALUES
(1, 1, 2, 'hello', '2014-12-05 08:36:10', 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `edit_log`
--

CREATE TABLE IF NOT EXISTS `edit_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `client_code` varchar(100) NOT NULL,
  `project_plan` tinyint(2) NOT NULL,
  `project_url` varchar(500) NOT NULL,
  `ftp_info` varchar(500) NOT NULL,
  `controlpanel_info` varchar(500) NOT NULL,
  `cms_info` varchar(500) NOT NULL,
  `other_info` varchar(500) NOT NULL,
  `project_description` text NOT NULL,
  `assigned_by` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT '1',
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `client_code`, `project_plan`, `project_url`, `ftp_info`, `controlpanel_info`, `cms_info`, `other_info`, `project_description`, `assigned_by`, `date_created`, `status`, `delete_status`, `ip`) VALUES
(1, 'test project', '0001234', 1, 'http://uuu.ddd', 'sdwdw`', 'eded', 'efefd', 'ded', 'dfwefhlwekfnjiwejdjwe', 1, '2014-12-05 08:35:41', 1, 2, '127.0.0.1'),
(2, 'test project', '0001234', 1, 'http://uuu.ddd', 'sdwdw`', 'eded', 'efefd', 'ded', 'dfwefhlwekfnjiwejdjwe', 1, '2014-12-05 08:35:45', 1, 1, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `project_plan`
--

CREATE TABLE IF NOT EXISTS `project_plan` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `project_plan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `project_plan`
--

INSERT INTO `project_plan` (`id`, `project_plan`) VALUES
(1, 'no plan selected');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `empcode` varchar(20) NOT NULL,
  `emptype` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_logout` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `online_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `empcode`, `emptype`, `email`, `password`, `date_created`, `last_login`, `last_logout`, `online_status`, `status`, `ip`) VALUES
(1, 'task', 'manager', '000', 1, 'sanjog152@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '2014-12-05 07:56:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, '127.0.0.1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
